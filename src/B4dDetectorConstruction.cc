//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: B4dDetectorConstruction.cc 101905 2016-12-07 11:34:39Z gunter $
//
/// \file B4dDetectorConstruction.cc
/// \brief Implementation of the B4dDetectorConstruction class

#include "B4dDetectorConstruction.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4GlobalMagFieldMessenger.hh"
#include "G4AutoDelete.hh"

#include "G4SDManager.hh"
#include "G4SDChargedFilter.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4PSTrackLength.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ThreadLocal
G4GlobalMagFieldMessenger* B4dDetectorConstruction::fMagFieldMessenger = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B4dDetectorConstruction::B4dDetectorConstruction()
 : G4VUserDetectorConstruction(),
   fCheckOverlaps(true)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B4dDetectorConstruction::~B4dDetectorConstruction()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* B4dDetectorConstruction::Construct()
{
  // Define materials
  DefineMaterials();

  // Define volumes
  return DefineVolumes();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B4dDetectorConstruction::DefineMaterials()
{
  // Use G4NistManager to define materials
  // (you can look them up at http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/ForApplicationDeveloper/html/Appendix/materialNames.html)
  auto nistManager = G4NistManager::Instance();
  nistManager->FindOrBuildMaterial("G4_AIR");
  nistManager->FindOrBuildMaterial("G4_NYLON-6-6");
  nistManager->FindOrBuildMaterial("G4_Ti");
  nistManager->FindOrBuildMaterial("G4_CR39");

  // Print materials
  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* B4dDetectorConstruction::DefineVolumes()
{
  // Geometry parameters
  G4double  bodySizeXY  = 10.*cm;
  G4double  implantSizeXY  = 10.*cm;
  G4double  detectorSizeXY  = 10.*cm;

  G4double  bodyThickness  = 6.*cm;
  G4double  implantThickness  = 2.*cm;
  G4double  detectorThickness  = 0.1*cm;

  auto  worldSizeXY = 1.2 * bodySizeXY;
  auto  worldSizeZ  = 2 * (bodyThickness+implantThickness+detectorThickness);

  // Get materials
  auto defaultMaterial = G4Material::GetMaterial("G4_AIR");
  auto bodyMaterial = G4Material::GetMaterial("G4_NYLON-6-6");
  auto implantMaterial = G4Material::GetMaterial("G4_Ti");
  auto detectorMaterial = G4Material::GetMaterial("G4_CR39");

  if (  ! defaultMaterial ||
        ! bodyMaterial ||
        ! implantMaterial ||
        ! detectorMaterial) {

    G4ExceptionDescription msg;
    msg << "Cannot retrieve materials already defined.";
    G4Exception("B4DetectorConstruction::DefineVolumes()",
      "MyCode0001", FatalException, msg);
  }


  //
  // World
  //
  G4cout << "----- Making the World ------" << G4endl;

  auto worldS
    = new G4Box("World",           // its name
                 worldSizeXY/2, worldSizeXY/2, worldSizeZ/2); // its size

  auto worldLV
    = new G4LogicalVolume(
                 worldS,           // its solid
                 defaultMaterial,  // its material
                 "World");         // its name

  auto worldPV
    = new G4PVPlacement(
                 0,                // no rotation
                 G4ThreeVector(),  // at (0,0,0)
                 worldLV,          // its logical volume
                 "World",          // its name
                 0,                // its mother  volume
                 false,            // no boolean operation
                 0,                // copy number
                 fCheckOverlaps);  // checking overlaps


  //
  // Body
  //
  G4cout << "----- Making the Body ------" << G4endl;

  auto bodyS
    = new G4Box("Body",     // its name
                 bodySizeXY/2, bodySizeXY/2, bodyThickness/2); // its size

  auto bodyLV
    = new G4LogicalVolume(
                 bodyS,    // its solid
                 defaultMaterial, // its material
                 "BodyLV");  // its name

  new G4PVPlacement(
                 0,                // no rotation
                 G4ThreeVector(),  // at (0,0,0)
                 bodyLV,          // its logical volume
                 "Body",    // its name
                 worldLV,          // its mother  volume
                 false,            // no boolean operation
                 0,                // copy number
                 fCheckOverlaps);  // checking overlaps


  //
  // Implant
  //
  G4cout << "----- Making the Implant ------" << G4endl;

  auto implantS
    = new G4Box("Implant",            // its name
                 implantSizeXY/2, implantSizeXY/2, implantThickness/2); // its size

  auto implantLV
    = new G4LogicalVolume(
                 implantS,        // its solid
                 implantMaterial, // its material
                 "ImplantLV");          // its name

   new G4PVPlacement(
                 0,                // no rotation
                 G4ThreeVector(0., 0., bodyThickness/2+implantThickness/2), //  its position
                 implantLV,       // its logical volume
                 "Implant",           // its name
                 worldLV,          // its mother  volume
                 false,            // no boolean operation
                 0,                // copy number
                 fCheckOverlaps);  // checking overlaps

  //
  // Detector
  //
  G4cout << "----- Making the Detector ------" << G4endl;

  auto detectorS
    = new G4Box("Detector",             // its name
                 detectorSizeXY/2, detectorSizeXY/2, detectorThickness/2); // its size

  auto detectorLV
    = new G4LogicalVolume(
                 detectorS,             // its solid
                 detectorMaterial,      // its material
                 "DetectorLV");      // its name

  new G4PVPlacement(
                 0,                // no rotation
                 G4ThreeVector(0., 0., bodyThickness/2 + implantThickness + detectorThickness/2), //  its position
                 detectorLV,            // its logical volume
                 "Detector",            // its name
                 worldLV,          // its mother  volume
                 false,            // no boolean operation
                 0,                // copy number
                 fCheckOverlaps);  // checking overlaps

  //
  // print parameters
  //
  G4cout
    << G4endl
    << "------------------------------------------------------------" << G4endl
    << "---> The phantom is made of "
    << bodyThickness/mm << "mm of " << bodyMaterial->GetName()
    << " + "
    << implantThickness/mm << "mm of " << implantMaterial->GetName() << " ] " << G4endl
    << "------------------------------------------------------------" << G4endl
    << "---> The detector is made of "
    << detectorThickness/mm << "mm of " << detectorMaterial->GetName();

  //
  // Visualization attributes
  //
  worldLV->SetVisAttributes (G4VisAttributes::GetInvisible());

  auto simpleBoxVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,1.0));
  simpleBoxVisAtt->SetVisibility(true);
  detectorLV->SetVisAttributes(simpleBoxVisAtt);

  //
  // Always return the physical World
  //
  return worldPV;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B4dDetectorConstruction::ConstructSDandField()
{
  G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  //
  // Scorers
  //

  // How much energy was deposited in the Body?
  // declare Body as a MultiFunctionalDetector scorer
  //
  auto bodyDetector = new G4MultiFunctionalDetector("Body");
  G4SDManager::GetSDMpointer()->AddNewDetector(bodyDetector);

  G4VPrimitiveScorer* primitive;
  primitive = new G4PSEnergyDeposit("Edep");
  bodyDetector->RegisterPrimitive(primitive);

  SetSensitiveDetector("BodyLV",bodyDetector);


  // How much energy was deposited in the Implant?
  // declare Implant as a MultiFunctionalDetector scorer
  //
  auto implantDetector = new G4MultiFunctionalDetector("Implant");
  G4SDManager::GetSDMpointer()->AddNewDetector(implantDetector);

  primitive = new G4PSEnergyDeposit("Edep");
  implantDetector->RegisterPrimitive(primitive);

  SetSensitiveDetector("ImplantLV",implantDetector);

  // How much energy was deposited in the Detector?
  // declare Detector as a MultiFunctionalDetector scorer
  //
  auto etchDetector = new G4MultiFunctionalDetector("Detector");
  G4SDManager::GetSDMpointer()->AddNewDetector(etchDetector);

  primitive = new G4PSEnergyDeposit("Edep");
  etchDetector->RegisterPrimitive(primitive);

  SetSensitiveDetector("DetectorLV",etchDetector);



  //
  // Magnetic field
  //
  // Create global magnetic field messenger.
  // Uniform magnetic field is then created automatically if
  // the field value is not zero.
  G4ThreeVector fieldValue;
  fMagFieldMessenger = new G4GlobalMagFieldMessenger(fieldValue);
  fMagFieldMessenger->SetVerboseLevel(1);

  // Register the field messenger for deleting
  G4AutoDelete::Register(fMagFieldMessenger);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
